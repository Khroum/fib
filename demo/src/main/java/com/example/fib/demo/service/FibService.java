package com.example.fib.demo.service;

public interface FibService {
    int [] nElements(int n);
}
