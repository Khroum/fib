package com.example.fib.demo.service;

import org.springframework.stereotype.Service;

@Service
public class FibServiceImpl implements FibService{
    @Override
    public int[] nElements(int n) {
       if (n<1){
           throw new IllegalArgumentException("n has to be equal or higher than 1");
       }

       int [] fibNumbers = new int [n];
       fibNumbers[0]=1;

       if(n==1){
           return fibNumbers;
       }

       fibNumbers[1]=1;

       for (int i=2; i<n;i++){
           fibNumbers[i] = fibNumbers[i-1] + fibNumbers[i-2];
       }
       return fibNumbers;
    }
}
