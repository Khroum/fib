package com.example.fib.demo.controller;

import com.example.fib.demo.service.FibService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FibController {
    @Autowired
    FibService fibService;

    @CrossOrigin
    @GetMapping("fib/{n}")
    ResponseEntity<?> getNumbers(@PathVariable Integer n){
        try {
            return ResponseEntity.ok(fibService.nElements(n));
        }catch (IllegalArgumentException iae){
            return ResponseEntity.badRequest().body(iae.getMessage());
        }
    }
}
